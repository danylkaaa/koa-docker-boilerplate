const JoiBase = require('joi');

const Joi = JoiBase.extend([
  require('./Password')
]);

exports.Joi = Joi;
exports.pagination = {
  page: Joi.number()
    .positive()
    .optional()
    .default(1),
  limit: Joi.number()
    .positive()
    .max(200)
    .optional()
    .default(25)
};
