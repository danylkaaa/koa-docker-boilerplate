const jwt = require('jsonwebtoken');

module.exports.sign = (payload, key, opts) => new Promise((resolve, reject) => {
    jwt.sign(payload, key, opts, (err, token) => {
      if (err) {
        return reject(err);
      }
      return resolve(token);
    });
  });

module.exports.verify = (token, key, opts) => new Promise((resolve, reject) => {
    jwt.verify(token, key, opts, (err, payload) => {
      if (err) {
        return reject(err);
      }
      return resolve(payload);
    });
  });
