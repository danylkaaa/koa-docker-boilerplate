/* eslint-disable  import/no-unresolved */
/**
 * @typedef {Object} KnexConnectConfig
 * @property {string} dialect - The database version (mysql, mysql2, pg)
 * @property {string} databaseVersion - The version of databse should be used (ex. 5.7 for mysql)
 * @property {string} host - The database address
 * @property {string|number} port - The port for connection
 * @property {string} user - The user's login from database
 * @property {string} password - The user's password from database
 * @property {string} database - The database name to use as default
 * @property {number} poolMin - The minimum number of connection in a pool
 * @property {number} poolMax - The maximum number of connection in a pool
 */
/**
 * @typedef {Object} RedisConnectConfig
 * @property {string} host - The database address
 * @property {number} port - The port for connection
 * @property {string} auth - The password from database
 */
/**
 * @typedef {Object} MongooseConnectConfig
 * @property {string} uri - The URI address
 * @property {string} user - The user from database
 * @property {string} password - The password from database
 */

/**
 * @see https://knexjs.org/#Installation-client
 * @param {KnexConnectConfig} config - connection database options
 * @returns {Knex.QueryBuilder}
 */
module.exports.getKnex = (config) => {
  const knex = require('knex');
  const { knexSnakeCaseMappers } = require('objection');
  console.debug('connect to knex with config %o', config);
  return knex({
    client: config.dialect,
    version: config.databaseVersion,
    connection: {
      host: config.host,
      port: config.port,
      user: config.user,
      password: config.password,
      database: config.database
    },
    pool: {
      min: config.poolMin || 1,
      max: config.poolMax || 10
    },
    ...knexSnakeCaseMappers()
  });
};

/**
 * @see https://www.npmjs.com/package/redis#rediscreateclient
 * @param {RedisConnectConfig} config
 * @returns {RedisClient}
 */
module.exports.getRedis = (config) => {
  const redis = require('redis');
  console.debug('connect to redis with config %O', config);
  return redis.createClient(config);
};

/**
 * @see https://github.com/Automattic/kue#creating-jobs
 * @param {RedisConnectConfig}|RedisClient} redisClient
 * @returns {Queue}
 */
module.exports.getQueue = (redisClient) => {
  const kue = require('kue');
  console.debug('connect to queue with config %O', redisClient);
  return kue.createQueue({
    redis: redisClient,
  });
};

/**
 * @see https://mongoosejs.com/docs/api.html#mongoose_Mongoose-connect
 * @param {MongooseConnectConfig} config
 * @return {Promise<void>}
 */
exports.mongoose = (config) => {
  const mongoose = require('mongoose');
  mongoose.Promise = Promise;
  return mongoose
    .connect(
      config.uri,
      {
        pass: config.password,
        user: config.user,
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true
      }
    )
    .then(() => {
      console.info('connected to MongoDB');
    });
};
