const { ExtractJwt, Strategy } = require('passport-jwt');

const authenticate = async (req, jwtPayload, done) => {
  try {
    const user = await req.ctx.store.User.query().findById(jwtPayload.uid);
    if (user) {
      done(null, user);
    } else {
      done('This user was deleted', false);
    }
  } catch (e) {
    done(null, false);
  }
};

module.exports.create = (secretOrKey) => new Strategy({
  passReqToCallback: true,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey
}, authenticate);
