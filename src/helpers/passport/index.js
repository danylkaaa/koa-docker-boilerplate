module.exports = {
  UserBasicStrategy: require('./UserBasicStrategy'),
  UserJWTStrategy: require('./UserJWTStrategy'),
  GoogleTokenStrategy: require('./GoogleTokenStrategy')
};
