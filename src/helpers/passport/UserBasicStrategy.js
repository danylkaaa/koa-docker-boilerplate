const { BasicStrategy } = require('passport-http');
const passwordsTools = require('../passwords');

const authenticate = async (req, email, password, done) => {
  try {
    const user = await req.ctx.store.User.findByEmail(email)
      .select('email', 'password', 'uid');
    if (user && await await passwordsTools.comparePasswords(password, user.password)) {
      done(null, await req.ctx.store.User.query()
        .findById(user.uid));
    } else {
      done('Invalid email or password', false);
    }
  } catch (e) {
    done(null, false);
  }
};

module.exports = new BasicStrategy({ passReqToCallback: true }, authenticate);
