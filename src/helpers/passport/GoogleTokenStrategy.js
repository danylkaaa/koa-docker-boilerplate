const { Strategy } = require('passport-token-google');

const authenticate = async (req, accessToken, refreshToken, profile, done) => {
  try {
    // const user = await req.ctx.store.User.findByGoogleId(email)
    //   .select('email', 'password', 'uid');
    // if (user && await await passwordsTools.comparePasswords(password, user.password)) {
    //   done(null, await req.ctx.store.User.query()
    //     .findById(user.uid));
    // } else {
    done('Unauthorized', false);
    // }
  } catch (e) {
    done(null, false);
  }
};

module.exports.create = (clientID, clientSecret) => new Strategy({
  clientID,
  clientSecret,
  passReqToCallback: true,
}, authenticate);
