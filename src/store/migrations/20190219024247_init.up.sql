CREATE OR REPLACE FUNCTION trigger_set_TIMESTAMPTZ()
  RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW();
  RETURN NEW;
END;
$$
LANGUAGE plpgsql;

CREATE TABLE users (
  uid       VARCHAR(36)             NOT NULL
    CONSTRAINT users_pk
      PRIMARY KEY,
  email     TEXT                    NOT NULL,
  password  TEXT                    NOT NULL,
  created_at TIMESTAMP DEFAULT now(),
  updated_at TIMESTAMP DEFAULT now() NOT NULL
);

CREATE UNIQUE INDEX users_email_uindex
  ON users (email);

CREATE UNIQUE INDEX users_uid_uindex
  ON users (uid);

CREATE TRIGGER set_timestamp_users
  BEFORE UPDATE
  ON users
  FOR EACH ROW
EXECUTE PROCEDURE trigger_set_TIMESTAMPTZ();
