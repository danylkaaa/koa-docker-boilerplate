const { Model } = require('objection');
const constants = require('../constants');
const helpers = require('../helpers');
const { pick } = require('lodash');
const uuid = require('uuid/v4');

class User extends Model {
  static get idColumn() {
    return 'uid';
  }

  static get tableName() {
    return 'users';
  }

  static get jsonSchema() {
    return {
      type: 'object',
      required: [
        'email',
        'password'
      ],
      properties: {
        uid: { type: 'string' },
        password: { type: 'string' },
        role: { type: 'string' },
        createdAt: { type: 'string' },
        updatedAt: { type: 'string' }
      }
    };
  }

  async $beforeInsert(queryContext) {
    this[User.idColumn] = uuid();
    this.password = await helpers.passwords.encryptPassword(this.password, constants.passwordSaltRounds);
    await super.$beforeInsert(queryContext);
  }

  async $beforeUpdate(queryContext) {
    this.password = await helpers.passwords.encryptPassword(this.password, constants.passwordSaltRounds);
    await super.$beforeUpdate(queryContext);
  }

  static async isEmailTaken(email, { trx } = {}) {
    const result = await User.query(trx)
      .where({ email })
      .count();
    return !result[0].count;
  }

  static findByEmail(email) {
    return User.query()
      .findOne({ email });
  }

  toPrivateJson() {
    return pick(this, ['uid', 'email', 'createdAt', 'updatedAt']);
  }

  toPublicJson() {
    return pick(this, ['uid', 'createdAt', 'updatedAt']);
  }
}

module.exports = User;
