const Router = require('koa-router');
const { middleware: validation } = require('@zulus/request-validator');
const registerHandler = require('../handlers/auth/register');
const loginHandler = require('../handlers/auth/login');
const getAccessToken = require('../handlers/auth/getAccessToken');
const { authenticate } = require('../middlewares/auth');

const router = Router();

router.post('/register', validation(registerHandler.schema), registerHandler);
router.post('/login', authenticate('local'), loginHandler);
router.get('/token', authenticate('jwt-refresh'), getAccessToken);
router.get('/google', authenticate('google-token'), registerHandler);

module.exports = router;
