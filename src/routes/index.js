const Router = require('koa-router');
const ping = require('../handlers/ping');
const authRouter = require('./auth');

const router = Router();
const apiRouter = Router();

apiRouter.get('/ping', ping);
apiRouter.use('/auth', authRouter.routes());
apiRouter.use('/auth', authRouter.allowedMethods());

router.use('/api/v1', apiRouter.routes());
router.use('/api/v1', apiRouter.allowedMethods());

module.exports = router;
