module.exports.user = {
  password: {
    min: 4,
    max: 20,
    regexp: /^(?=.*\d)(?=.*[A-Za-z]).*$/,
  }
};
