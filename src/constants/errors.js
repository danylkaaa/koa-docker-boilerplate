const httpStatus = require('http-status');

module.exports.UNAUTHORIZED = {
  statusCode: 401,
  code: httpStatus['401_NAME'],
  message: 'Authentication is needed to access the requested endpoint.'
};

module.exports.UNKNOWN_ENDPOINT = {
  statusCode: 404,
  code: httpStatus['404_NAME'],
  message: 'The requested endpoint does not exist.'
};

module.exports.UNKNOWN_RESOURCE = {
  statusCode: 404,
  code: httpStatus['404_NAME'],
  message: 'The specified resource was not found.'
};

module.exports.INVALID_REQUEST = {
  statusCode: 422,
  code: httpStatus['422_NAME'],
  message: 'The request has invalid parameters.'
};


/**
 * Server Errors
 */
module.exports.INTERNAL_ERROR = {
  statusCode: 500,
  code: httpStatus['500_NAME'],
  message: 'The server encountered an internal error.'
};

module.exports.UNKNOWN_ERROR = {
  statusCode: 500,
  code: httpStatus['500_NAME'],
  message: 'The server encountered an unknown error.'
};
