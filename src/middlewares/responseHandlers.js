const statusCodes = require('http-status');

const clearEmptyBodyFields = (ctx) => {
  if (ctx.body) {
    Object.keys(ctx.body)
      .forEach(key => {
        if (ctx.body[key] === null || ctx.body[key] === undefined) {
          delete ctx.body[key];
        }
      });
  }
};

module.exports = () => async (ctx, next) => {
  ctx.res.statusCodes = statusCodes;
  ctx.statusCodes = ctx.res.statusCodes;

  ctx.res.success = ({ statusCode, data = null, message = null } = {}) => {
    const status = 'ok';
    if (!!statusCode && (statusCode < 400)) {
      ctx.status = statusCode;
    } else if (!(ctx.status < 400)) {
      ctx.status = statusCodes.OK;
    }
    ctx.body = {
      status,
      data,
      message,
      statusCode
    };
  };
  ctx.res.ok = ctx.res.success;
  ctx.res.error = ({ statusCode, code, data = null, message = null } = {}) => {
    const status = 'error';
    if (!!statusCode && (statusCode >= 400 && statusCode < 600)) {
      ctx.status = statusCode;
    } else if (!(ctx.status >= 500 && ctx.status < 600)) {
      ctx.status = statusCodes.INTERNAL_SERVER_ERROR;
    }
    if (!code) {
      code = statusCodes[`${ctx.status}_NAME`];
    }
    ctx.body = {
      status,
      code,
      data,
      message,
      statusCode
    };
  };
  ctx.res.accepted = (params = {}) => {
    ctx.res.success({
      ...params,
      statusCode: statusCodes.ACCEPTED
    });
  };

  ctx.res.noContent = (params = {}) => {
    ctx.res.success({
      ...params,
      statusCode: statusCodes.NO_CONTENT
    });
  };

  ctx.res.badRequest = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.BAD_REQUEST
    });
  };

  ctx.res.unauthorized = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.UNAUTHORIZED
    });
  };

  ctx.res.forbidden = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.FORBIDDEN
    });
  };

  ctx.res.notFound = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.NOT_FOUND
    });
  };

  ctx.res.requestTimeout = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.REQUEST_TIMEOUT
    });
  };

  ctx.res.unprocessableEntity = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.UNPROCESSABLE_ENTITY
    });
  };

  ctx.res.internalServerError = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.INTERNAL_SERVER_ERROR
    });
  };

  ctx.res.notImplemented = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.NOT_IMPLEMENTED
    });
  };

  ctx.res.badGateway = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.BAD_GATEWAY
    });
  };

  ctx.res.serviceUnavailable = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.SERVICE_UNAVAILABLE
    });
  };

  ctx.res.gatewayTimeOut = (params = {}) => {
    ctx.res.error({
      ...params,
      statusCode: statusCodes.GATEWAY_TIMEOUT
    });
  };

  await next();
  clearEmptyBodyFields(ctx);
};
