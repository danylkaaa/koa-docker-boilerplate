const passport = require('koa-passport');
const config = require('../../config/server');
const { UserBasicStrategy, UserJWTStrategy, GoogleTokenStrategy } = require('../helpers/passport');
const fse = require('fs-extra');

const googleOAuth2Creds = fse.readJsonSync(config.oauth2CredsPaths.google).installed;

passport.use('local', UserBasicStrategy);
passport.use('jwt-access', UserJWTStrategy.create(config.jwt.access.secret));
passport.use('jwt-refresh', UserJWTStrategy.create(config.jwt.refresh.secret));
passport.use('google-token', GoogleTokenStrategy.create(googleOAuth2Creds.client_id, googleOAuth2Creds.client_secret));

module.exports.authenticate = (strategies, opts) => async (ctx, next) => {
  await passport.authenticate(strategies, {
    ...opts,
    session: false
  }, async (err, user) => {
    if (!user) {
      ctx.res.unauthorized({ message: err });
    } else {
      ctx.state.user = user;
      await next();
    }
  })(ctx, next);
};

module.exports.initialize = (opts) => passport.initialize(opts);
