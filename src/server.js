const config = require('../config/server');
const App = require('./App');

const logger = require('log4js')
  .getLogger('server');

const app = new App(config);

function handleError(err) {
  logger.fatal('Unhandled exception occured %s', err);
}

async function terminate(signal) {
  try {
    await app.terminate();
  } finally {
    logger.info('App is terminated with signal: %s', signal);
    process.kill(process.pid, signal);
  }
}

// Handle uncaught errors
app.on('error', handleError);
// Start server
if (!module.parent) {
  const server = app.listen(config.app.port, () => {
    logger.info(`API server listening on ${config.app.port}, in ${config.env}`);
  });
  server.on('error', handleError);

  const errors = ['unhandledRejection', 'uncaughtException'];
  errors.forEach((error) => {
    process.on(error, handleError);
  });

  const signals = ['SIGTERM', 'SIGINT', 'SIGUSR2'];
  signals.forEach(signal => {
    process.once(signal, () => terminate(signal));
  });
}

module.exports = app;
