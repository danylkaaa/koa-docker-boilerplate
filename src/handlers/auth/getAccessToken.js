const { auth } = require('../../helpers');

const getAccessToken = async (ctx) => {
  const { user } = ctx.state;
  const token = await auth.getTokenForUser(user, ctx.config.jwt.access);
  ctx.res.ok({
    data: {
      tokens: {
        access: token
      }
    }
  });
};

module.exports = getAccessToken;
