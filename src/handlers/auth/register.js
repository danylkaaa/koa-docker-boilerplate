const { Joi } = require('../../helpers/JoiObjects/validation-objects');
const { auth } = require('../../helpers');

const register = async (ctx) => {
  const body = ctx.request.body;
  if (await ctx.store.User.isEmailTaken(body.email)) {
    return ctx.res.badRequest({ message: 'This email is already taken' });
  }
  await ctx.store.User.query()
    .insert(body);
  const user = await ctx.store.User.findByEmail(body.email);
  const tokens = await auth.getTokensForUser(user, ctx.config.jwt);
  return ctx.res.ok({
    data: {
      user: user.toPrivateJson(),
      tokens
    }
  });
};

register.schema = {
  body: Joi.object({
    email: Joi.string()
      .trim()
      .email(),
    password: Joi.password()
      .isPassword()
  })
};

module.exports = register;
